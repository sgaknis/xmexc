<?php

/**
 * Responsible for initializing form data and form handling
 * Class FormController
 */
class FormController extends BaseController
{

    /**
     * Initializes company data and shows the form
     * @return mixed
     */
    public function showForm()
    {

        $company = new Company();

        $company_data = $company->get_company_data();

        return View::make('form', compact('company_data'));
    }


    public function validateForm()
    {

        $rules = array(
            'symbol' => 'required|not_in:0',
            'startdate' => 'required|date',
            'enddate' => 'required|date|after:startdate',
            'email' => 'required|email'
        );

        $messages = array(
            'enddate.after' => 'The End Date may not be smaller than Start Date ',
        );

        $validator = Validator::make(Input::all(), $rules, $messages);

        if ($validator->fails()) {
            return Redirect::route('form.show')->withErrors($validator)->withInput();
        }


        Session::put(array(
                'formInput' => array(
                    'symbol' => Input::get('symbol'),
                    'from' => Input::get('startdate'),
                    'to' => Input::get('enddate'),
                    'email' => Input::get('email')
                )
            )
        );

        return Redirect::route('quotes.show');

    }


}
