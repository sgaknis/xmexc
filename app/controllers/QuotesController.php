<?php

/**
 * Handles data for the results screen
 * Class QuotesController
 */
class QuotesController extends BaseController
{
    /**
     * Main controller function
     * @return mixed
     */
    public function showQuotes()
    {
        // If session formInput not set Redirect to form
        if (!Session::has('formInput')) {
            return Redirect::route('form.show');
        }

        // Init a new Quote Object with form Input
        $quotes = new Quote(Session::get('formInput'));

        // Attempt to set quotes from Api
        try {
            $quotes->set_historical_quotes();
        } catch (ErrorException $exception) {
            $msg = 'The service is unavailable for the chosen dates. Please choose another set of dates.';
            return Redirect::route('form.show')->with("exception.message", $msg)->withInput(Session::get('formInput'));
        }

        // Get the historical quotes for use in the view
        $historical_quotes = $quotes->get_historical_quotes();

        // Generate the data to use in the chart
        $quotes->generate_chart_data();

        // Send mail with results
        $quotes->send_mail();

        return View::make('quotes', compact('historical_quotes', 'input'));

    }

}
