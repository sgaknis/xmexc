<?php

/**
 * Provides symbols and company names
 * Class Company
 */
class Company
{

    private $csv_path = '/files/companylist.csv';

    protected $company_data = array();

    /**
     * main class constructor
     * @throws Exception
     */
    public function __construct()
    {
        $this->set_data_from_csv();
    }

    /**
     * Reads csv and sets the data
     * @throws Exception
     */
    private function set_data_from_csv()
    {

        if (!file_exists(base_path() . $this->csv_path)) {
            throw new Exception(__CLASS__ . '->' . __FUNCTION__ . ': csv does not exist.');
        }

        $file = fopen(base_path() . $this->csv_path, "r");

        while (!feof($file)) {

            $line = fgetcsv($file);

            // Omit the first line
            if ($line[0] === 'Symbol') {
                $this->company_data[0] = 'Make a selection';
                continue;
            }

            $this->company_data[$line[0]] = $line[1];
        }

        fclose($file);
    }

    /**
     * Returns the company data
     * @return array
     */
    public function get_company_data()
    {
        return $this->company_data;
    }

    /**
     * Returns the companyname for given symbol
     * @return array
     */
    public function get_company_name($symbol)
    {
        return $this->company_data[$symbol];
    }

}