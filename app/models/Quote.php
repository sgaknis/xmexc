<?php

/**
 * Created by Stathis Gaknis.
 * Date: 27/1/2015
 * Time: 7:08 μμ
 */

class Quote extends YahooApiConnector
{

    protected $hq = array();

    protected $email;

    public function __construct($formInput)
    {
        parent::__construct($formInput);

        $this->set_email($formInput);
    }


    /**
     * Sets the e-mail
     * @param $formInput
     * @throws Exception
     */
    private function set_email($formInput)
    {
        if (isset($formInput['email']) && !empty($formInput['email'])) {
            $this->email = $formInput['email'];
        } else {
            throw new Exception(__CLASS__ . '->' . __FUNCTION__ . ': formInput "email" input error');
        }
    }

    /**
     * Sets historical quotes array
     * @return array
     */
    public function set_historical_quotes()
    {

        // Open file to save the results
        $file = fopen(base_path() . '/files/results.csv', "w");

        // Init array to get keys
        $quotes_array_keys = array();

        // Get csv contents from API
        $contents = fopen($this->hq_fullurl, "r");

        while (!feof($contents)) {

            $line = fgetcsv($contents);

            if (is_array($line) && !empty($line)) {

                fputcsv($file, $line);

                //Init array keys
                if ($line[0] === 'Date') {

                    $quotes_array_keys = $line;

                } else {

                    foreach ($line as $key => $val) {

                        $q_array[$quotes_array_keys[$key]] = $val;

                    }

                    array_push($this->hq, $q_array);

                }
            }
        }

        fclose($file);
        fclose($contents);

    }

    /**
     * Returns the historical Quotes
     * @return array
     */
    public function get_historical_quotes()
    {
        return $this->hq;
    }

    /**
     * Update/Save Chart data
     */
    public function generate_chart_data()
    {

        $chart_data = array();

        foreach ($this->hq as $quote) {

            array_push($chart_data, array($quote['Date'], $quote['Open'], $quote['Close']));

        }

        file_put_contents(base_path() . "/files/data.json", json_encode($chart_data));

    }

    /**
     * Sends email with results
     */
    public function send_mail()
    {
        // Get company name
        $company = new Company();
        $company_name = $company->get_company_name($this->symbol);

        // Get results attachement path
        $results_path = '';
        if (file_exists(base_path() . '/files/results.csv')) {
            $results_path = base_path() . '/files/results.csv';
        }

        Mail::send('emails.quotes', array('from' => $this->from, 'to' => $this->to), function ($message) use ($company_name, $results_path) {
            $message->to($this->email);
            $message->subject($company_name);
            if (!empty($results_path)) {
                $message->attach($results_path);
            }
        });

    }

}