<?php

/**
 * Created by Stathis Gaknis.
 * Date: 27/1/2015
 * Time: 7:10 μμ
 */
class YahooApiConnector
{
    /**
     * Historical Quotes base Url
     * @var string
     */
    private $hq_baseurl = 'http://ichart.yahoo.com/table.csv?s=';

    /**
     * Historical Quotes Full Url
     * @var
     */
    protected $hq_fullurl;

    /**
     * The symbol from user input
     * @var
     */
    protected $symbol;

    /**
     * The from date from user input
     * @var
     */
    protected $from;

    /**
     * The to date from user input
     * @var
     */
    protected $to;

    public function __construct($formInput)
    {
        $this->set_api_options($formInput);
        $this->generate_historical_quotes_url();
    }

    /**
     * Sets the Api required options from input
     * @param $formInput
     * @throws Exception
     */
    private function set_api_options($formInput)
    {

        if (isset($formInput['symbol']) && !empty($formInput['symbol'])) {
            $this->symbol = $formInput['symbol'];
        } else {
            throw new Exception(__CLASS__ . '->' . __FUNCTION__ . ': formInput "symbol" input error');
        }

        if (isset($formInput['from']) && !empty($formInput['from'])) {
            $this->from = $formInput['from'];
        } else {
            throw new Exception(__CLASS__ . '->' . __FUNCTION__ . ': formInput "from" input error');
        }

        if (isset($formInput['to']) && !empty($formInput['to'])) {
            $this->to = $formInput['to'];
        } else {
            throw new Exception(__CLASS__ . '->' . __FUNCTION__ . ': formInput "to" input error');
        }

    }

    /**
     * Generates historical quotes url based on input
     */
    private function generate_historical_quotes_url()
    {
        // Add the symbol
        $full_url = $this->hq_baseurl . $this->symbol;

        // Add from date
        $full_url .= '&a=' . $this->get_month($this->from) . '&b=' . $this->get_day($this->from) . '&c=' . $this->get_year($this->from);

        // Add to date
        $full_url .= '&d=' . $this->get_month($this->to) . '&e=' . $this->get_day($this->to) . '&f=' . $this->get_year($this->to);

        // Add interval
        $full_url .= '&g=d&ignore=.csv';

        $this->hq_fullurl = $full_url;
    }

    /**
     * returns the month minus 1
     * @return int
     */
    private function get_month($date)
    {
        $date_array = explode('/', $date);

        return $month = (int)$date_array[0] - 1;
    }

    /**
     * returns the day for given date
     * @param $date
     * @return int
     */
    private function get_day($date)
    {
        $date_array = explode('/', $date);

        return (int)$date_array[1];
    }

    /**
     * returns the year for given date
     * @param $date
     * @return int
     */
    private function get_year($date)
    {

        $date_array = explode('/', $date);

        return (int)$date_array[2];

    }


}