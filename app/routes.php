<?php

Route::get('/', array('as' => 'form.show', 'uses' => 'FormController@showForm'));

Route::post('/formvalidate', array('as' => 'form.post', 'uses' => 'FormController@validateForm'));

Route::get('/quotes', array('as' => 'quotes.show', 'uses' => 'QuotesController@showQuotes'));
