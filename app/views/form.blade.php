@extends('layouts.master')

    @section('content')

        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <h4>Fill in the form below</h4>
            </div>
        </div>
        <br>

        @if (Session::has('exception.message'))
        <div class="alert alert-danger">
            {{ Session::pull('exception.message') }}
        </div>
        @endif

      @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
                {{ $error }}<br>
            @endforeach
        </div>
        <br>
        <br>
        @endif
        {{ Form::open(
                array(
                    'route' => 'form.post',
                    'method' => 'post',
                    'id' => 'simpleform',
                    'class' => 'form-horizontal',
                    'novalidate' => '',
                    'autocomplete' => 'off'
                    )
                ) }}

            <fieldset>

                <!-- Symbol input -->
                <div class="form-group @if ($errors->has('symbol')) has-error @endif">

                    {{ Form::label('symbol', 'Company Symbol', array('class' => 'col-md-4 control-label')) }}

                    <div class="col-md-4">

                        <select id="symbol" name="symbol" class="form-control">

                            @foreach ($company_data as $symbol => $name)

                                @if ($symbol === 0)
                                    <option value="{{ $symbol }}">{{ $name }}</option>
                                @else
                                    <option value="{{ $symbol }}">{{ $symbol . '&nbsp;&nbsp;('.$name.')' }}</option>
                                @endif

                            @endforeach

                        </select>
                        <span class="glyphicon glyphicon-ok feedback-icon success"></span>
                        <span class="glyphicon glyphicon-remove feedback-icon fail"></span>
                    </div>

                    <div class="col-md-4">
                        <p class="error text-danger"></p>
                    </div>

                </div>

                <!-- Start Date input -->
                <div class="form-group @if ($errors->has('startdate')) has-error @endif">

                        {{ Form::label('startdate', 'Start Date', array('class' => 'col-md-4 control-label')) }}

                    <div class="col-md-4">
                        {{ Form::text('startdate', '', array('id' => 'startdate', 'placeholder' => 'mm/dd/yyyy', 'class' => 'form-control input-md datepicker')) }}
                        <span class="glyphicon glyphicon-ok feedback-icon success"></span>
                        <span class="glyphicon glyphicon-remove feedback-icon fail"></span>
                    </div>

                    <div class="col-md-4">
                        <p class="error text-danger">{{ $errors->first('startdate') }}</p>
                    </div>

                </div>

                <!-- End date input -->
                <div class="form-group @if ($errors->has('enddate')) has-error @endif">
                    {{ Form::label('enddate', 'End Date', array('class' => 'col-md-4 control-label')) }}

                    <div class="col-md-4">
                        {{ Form::text('enddate', '', array('id' => 'enddate', 'placeholder' => 'mm/dd/yyyy', 'class' => 'form-control input-md datepicker')) }}
                        <span class="glyphicon glyphicon-ok feedback-icon success"></span>
                        <span class="glyphicon glyphicon-remove feedback-icon fail"></span>
                    </div>

                    <div class="col-md-4">
                        <p class="error text-danger">
                            @foreach($errors->get('enddate') as $message)
                                {{ $message }}
                            @endforeach
                    </div>

                </div>

                <!-- Email Input-->
                <div class="form-group">
                    {{ Form::label('email', 'Email', array('class' => 'col-md-4 control-label')) }}

                    <div class="col-md-4">
                        {{ Form::text('email', '', array('id' => 'email', 'placeholder' => 'Give your e-mail', 'class' => 'form-control input-md')) }}
                        <span class="glyphicon glyphicon-ok feedback-icon success"></span>
                        <span class="glyphicon glyphicon-remove feedback-icon fail"></span>
                    </div>

                    <div class="col-md-4">
                        <p class="error text-danger"></p>
                    </div>
                </div>

                <!-- Submit Button -->
                <div class="form-group">
                  <label class="col-md-4 control-label" for="submit"></label>
                  <div class="col-md-4">

                    {{ Form::button('Submit', array('type' => 'submit', 'id' => 'submit', 'class' => 'btn btn-primary')) }}

                  </div>
                </div>

            </fieldset>

        {{ Form::close() }}

    @stop