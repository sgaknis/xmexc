<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>PHP Test Exercise for XM | Gaknis Stathis</title>
        <meta name="description" content="PHP Test Exercise for XM | Gaknis Stathis">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        {{ HTML::style('css/vendor/bootstrap.min.css') }}
        {{ HTML::style('css/vendor/jquery-ui.min.css') }}
        {{ HTML::style('css/vendor/jquery.idealforms.css') }}
        {{ HTML::style('css/main.css') }}
        {{ HTML::style('css/compiled.css') }}
        {{ HTML::script('js/vendor/modernizr-2.6.2-respond-1.1.0.min.js') }}

    </head>

    <body>

        <div class="container">

            <header>
                <h1>XM-Test Application</h1>
                <h2>authored by <em>Stathis Gaknis</em> for XM</h2>
            </header>

            <hr>

            <section class="main-content">
                @yield('content')
            </section>

            <hr>

            <footer>
            <p>Author: Gaknis Stathis for XM | 2015</p>
            </footer>

        </div> <!-- /container -->

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.1.min.js"><\/script>')</script>

        {{ HTML::script('js/vendor/bootstrap.min.js') }}
        {{ HTML::script('js/vendor/jquery-ui.min.js') }}
        {{ HTML::script('js/vendor/jquery.idealforms.min.js') }}
        {{ HTML::script('js/vendor/highcharts-custom.js') }}

        {{ HTML::script('js/main.js') }}

    </body>

</html>
