@extends('layouts.master')

    @section('content')

        <a href="{{route('form.show')}}" class="btn btn-primary pull-right">
            <span class="glyphicon glyphicon-circle-arrow-left" aria-hidden="true"></span>
            &nbsp;Return to form
        </a>

        <div class="clearfix"></div>

        <hr>

        <div id="container" style="min-width: 340px; height: 400px; margin: 0 auto"></div>

        <h3>Historical Quotes</h3>

        <hr>

        @if (count($historical_quotes) > 0 )

            <table class="table" id="hq-table">

              <thead>

                <tr>

                    @foreach($historical_quotes[0] as $key => $value)

                    <td>{{$key}}</td>

                    @endforeach
                </tr>

              </thead>

              <tbody>

                @foreach($historical_quotes as $quote)

                <tr>
                    @foreach($quote as $q_key => $q_val)

                      <td>{{$q_val}}</td>

                    @endforeach
                </tr>

                @endforeach

              </tbody>
            </table>

        @else
            <p>No results</p>
        @endif

    @stop