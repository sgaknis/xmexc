$(function () {

    /*
     |--------------------------------------------------------------------------
     | Init Form Validation
     |--------------------------------------------------------------------------
     */
    $('form#simpleform').idealforms({
        field: '.form-group',
        error: '.error',
        validClass: 'has-success',
        invalidClass: 'has-error',
        iconHtml: false,
        silentLoad: true,
        rules: {
            'symbol': 'select:0',
            'startdate': 'required date',
            'enddate': 'required date',
            'email': 'required email'
        },
        onValidate: function (input, valid, rule) {

            if (rule === false) {
                var parent = $("#" + input.id).parents('.form-group');
                parent.find('.error').removeClass('hidden');
            }
        },
        onSubmit: function (invalid, e) {

            if (invalid) {
                e.preventDefault();
            }
        }
    });

    /*
     |--------------------------------------------------------------------------
     | Customize Datepicker
     |--------------------------------------------------------------------------
     */

    $('.datepicker').datepicker('option', 'maxDate', new Date);

    /*
     |--------------------------------------------------------------------------
     | Init Chart
     |--------------------------------------------------------------------------
     */

    $.getJSON("../files/data.json", function (data) {

        var opendata = [];
        var closedata = [];

        $.each(data, function (key, val) {

            var date_arr = val[0].split('-');
            var year = parseInt(date_arr[0]);
            var month = parseInt(date_arr[1]) - 1;
            var day = parseInt(date_arr[2]);
            var open = parseFloat(val[1]);
            var close = parseFloat(val[2]);

            opendata.push([Date.UTC(year, month, day), open]);
            closedata.push([Date.UTC(year, month, day), close]);
        });

        var open = opendata.reverse();
        var close = closedata.reverse();

        $('#container').highcharts({
            title: {
                text: 'Historical Quotes'
            },
            subtitle: {
                text: 'Open/Close Values'
            },
            exporting: {
                type: 'image/jpeg'
            },
            xAxis: {
                type: 'datetime',
                dateTimeLabelFormats: {
                    day: '%e of %b'
                }
            },
            series: [{
                name: 'Open',
                data: open
            },
                {
                    name: 'Close',
                    data: close
                }
            ]
        });

    });

});

